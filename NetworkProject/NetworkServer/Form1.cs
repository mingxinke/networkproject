﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.IO;
using System.Net;
using System.Net.Sockets;

namespace NetworkServer
{
    public partial class from_mian : Form
    {
        #region 常量定义
        private TcpListener tcpListener = null;
        private IPAddress ipAddress;
        private List<NetTcpClient> clientList = null;

        //委托
        private delegate void ShowStatus(string str);
        private ShowStatus showStatusCallback;

        #endregion

        public from_mian()
        {
            InitializeComponent();

            clientList = new List<NetTcpClient>();

            showStatusCallback = new ShowStatus(showStatus);

            ipAddress = IPAddress.Loopback;
            tBox_ip.Text = ipAddress.ToString();
        }

        /// <summary>
        /// 显示状态
        /// </summary>
        /// <param name="str"></param>
        private void showStatus(string str)
        {
            toolStripLab_Status.Text = str;
        }

        private void btn_startListen_Click(object sender, EventArgs e)
        {
            int nPort = int.Parse(tBox_port.Text);
            tcpListener = new TcpListener(ipAddress, nPort);
            tcpListener.Start();
            Thread thread = new Thread(listenClientConnect);
            thread.Start();
        }

        /// <summary>
        /// 监听客户端的连接
        /// </summary>
        private void listenClientConnect()
        {
            statusStrip_menu.Invoke(showStatusCallback, "正在监听...");
            try
            {
                TcpClient tcpClient = tcpListener.AcceptTcpClient();
                NetTcpClient netTcpClient = new NetTcpClient();
                netTcpClient.conState += clientConnectStateChange;
                netTcpClient.startConnected(tcpClient);
                clientList.Add(netTcpClient);
            }
            catch (System.Exception ex)
            {
                Console.WriteLine(ex.ToString());
                statusStrip_menu.Invoke(showStatusCallback, "停止监听");
            }

        }

        private void clientConnectStateChange(Object send, ConStateEventArgs e)
        {
            NetTcpClient netCLient = send as NetTcpClient;
            if (netCLient == null)
            {
                return;
            }
            if (e.bConnected)
            {
                IPEndPoint ipPort = netCLient.TcpCLient.Client.RemoteEndPoint as IPEndPoint;
                ListViewItem listItem;
                listItem = listView_clientList.Items.Add(ipPort.Address.ToString());
                listItem.SubItems.Add(ipPort.Port.ToString());
                listItem.EnsureVisible();
            }
            else
            {

            }

        }

        private void from_mian_Load(object sender, EventArgs e)
        {

        }

        private void btn_closeListen_Click(object sender, EventArgs e)
        {
            if (tcpListener != null)
            {
                tcpListener.Stop();
                statusStrip_menu.Invoke(showStatusCallback, "停止监听");
            }
        }
    }
}
