﻿namespace NetworkServer
{
    partial class from_mian
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_startListen = new System.Windows.Forms.Button();
            this.lab_port = new System.Windows.Forms.Label();
            this.btn_closeListen = new System.Windows.Forms.Button();
            this.tBox_port = new System.Windows.Forms.TextBox();
            this.statusStrip_menu = new System.Windows.Forms.StatusStrip();
            this.toolStripLab_Status = new System.Windows.Forms.ToolStripStatusLabel();
            this.listView_clientList = new System.Windows.Forms.ListView();
            this.colHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.label1 = new System.Windows.Forms.Label();
            this.tBox_ip = new System.Windows.Forms.TextBox();
            this.statusStrip_menu.SuspendLayout();
            this.SuspendLayout();
            // 
            // btn_startListen
            // 
            this.btn_startListen.Location = new System.Drawing.Point(318, 12);
            this.btn_startListen.Name = "btn_startListen";
            this.btn_startListen.Size = new System.Drawing.Size(75, 23);
            this.btn_startListen.TabIndex = 1;
            this.btn_startListen.Text = "开始监听";
            this.btn_startListen.UseVisualStyleBackColor = true;
            this.btn_startListen.Click += new System.EventHandler(this.btn_startListen_Click);
            // 
            // lab_port
            // 
            this.lab_port.AutoSize = true;
            this.lab_port.Location = new System.Drawing.Point(167, 16);
            this.lab_port.Name = "lab_port";
            this.lab_port.Size = new System.Drawing.Size(53, 12);
            this.lab_port.TabIndex = 2;
            this.lab_port.Text = "监听端口";
            // 
            // btn_closeListen
            // 
            this.btn_closeListen.Location = new System.Drawing.Point(399, 12);
            this.btn_closeListen.Name = "btn_closeListen";
            this.btn_closeListen.Size = new System.Drawing.Size(75, 23);
            this.btn_closeListen.TabIndex = 3;
            this.btn_closeListen.Text = "关闭监听";
            this.btn_closeListen.UseVisualStyleBackColor = true;
            this.btn_closeListen.Click += new System.EventHandler(this.btn_closeListen_Click);
            // 
            // tBox_port
            // 
            this.tBox_port.Location = new System.Drawing.Point(242, 12);
            this.tBox_port.Name = "tBox_port";
            this.tBox_port.Size = new System.Drawing.Size(70, 21);
            this.tBox_port.TabIndex = 4;
            this.tBox_port.Text = "52521";
            // 
            // statusStrip_menu
            // 
            this.statusStrip_menu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripLab_Status});
            this.statusStrip_menu.Location = new System.Drawing.Point(0, 488);
            this.statusStrip_menu.Name = "statusStrip_menu";
            this.statusStrip_menu.Size = new System.Drawing.Size(484, 22);
            this.statusStrip_menu.TabIndex = 5;
            this.statusStrip_menu.Text = "statusStrip1";
            // 
            // toolStripLab_Status
            // 
            this.toolStripLab_Status.Name = "toolStripLab_Status";
            this.toolStripLab_Status.Size = new System.Drawing.Size(32, 17);
            this.toolStripLab_Status.Text = "就绪";
            // 
            // listView_clientList
            // 
            this.listView_clientList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colHeader1,
            this.colHeader2});
            this.listView_clientList.Location = new System.Drawing.Point(15, 41);
            this.listView_clientList.Name = "listView_clientList";
            this.listView_clientList.Size = new System.Drawing.Size(461, 435);
            this.listView_clientList.TabIndex = 6;
            this.listView_clientList.UseCompatibleStateImageBehavior = false;
            this.listView_clientList.View = System.Windows.Forms.View.Details;
            // 
            // colHeader1
            // 
            this.colHeader1.Text = "客户端IP";
            this.colHeader1.Width = 239;
            // 
            // colHeader2
            // 
            this.colHeader2.Text = "客户端端口";
            this.colHeader2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.colHeader2.Width = 200;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 12);
            this.label1.TabIndex = 7;
            this.label1.Text = "IP地址";
            // 
            // tBox_ip
            // 
            this.tBox_ip.Location = new System.Drawing.Point(61, 13);
            this.tBox_ip.Name = "tBox_ip";
            this.tBox_ip.Size = new System.Drawing.Size(100, 21);
            this.tBox_ip.TabIndex = 8;
            // 
            // from_mian
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(484, 510);
            this.Controls.Add(this.tBox_ip);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.listView_clientList);
            this.Controls.Add(this.statusStrip_menu);
            this.Controls.Add(this.tBox_port);
            this.Controls.Add(this.btn_closeListen);
            this.Controls.Add(this.lab_port);
            this.Controls.Add(this.btn_startListen);
            this.Name = "from_mian";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.from_mian_Load);
            this.statusStrip_menu.ResumeLayout(false);
            this.statusStrip_menu.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btn_startListen;
        private System.Windows.Forms.Label lab_port;
        private System.Windows.Forms.Button btn_closeListen;
        private System.Windows.Forms.TextBox tBox_port;
        private System.Windows.Forms.StatusStrip statusStrip_menu;
        private System.Windows.Forms.ToolStripStatusLabel toolStripLab_Status;
        private System.Windows.Forms.ListView listView_clientList;
        private System.Windows.Forms.ColumnHeader colHeader1;
        private System.Windows.Forms.ColumnHeader colHeader2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tBox_ip;
    }
}

