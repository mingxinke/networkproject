﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Threading;

namespace NetworkServer
{
    class NetTcpClient
    {

        private bool bConnected = false;
        public bool BConnected {
            get{ return bConnected; }
        }
        private object lockObj = new object();

        //网络连接
        private TcpClient tcpCLient = null;

        public TcpClient TcpCLient
        {
            get
            {
                return tcpCLient;
            }
        }

        //读取流
        private BinaryReader reader = null;

        public BinaryReader Reader
        {
            get
            {
                return reader;
            }
        }

        //写入流
        private BinaryWriter writer = null;

        public BinaryWriter Writer
        {
            get
            {
                return writer;
            }
        }

        //线程
        private Thread thread = null;

        public Thread Thread
        {
            get
            {
                return thread;
            }
        }

        #region 创建代理
        public delegate void ConStateEventHandler(Object send, ConStateEventArgs e);
        public event ConStateEventHandler conState;        //事件

        private void OnConState(ConStateEventArgs e)
        {
            conState?.Invoke(this, e);
        }


        public delegate void ReaderMsgHandler(object obj, string sMsg);
        public event ReaderMsgHandler readerMsg;

        private void OnReaderMsg(string str)
        {
            readerMsg?.Invoke(this, str);
        }
        #endregion

        public NetTcpClient()
        {
        }

        public void startConnected(TcpClient newClient)
        {
            bConnected = true;
            tcpCLient = newClient;
            if (tcpCLient != null)
            {
                try
                {
                    OnConState(new ConStateEventArgs(bConnected));
                    NetworkStream netStream = tcpCLient.GetStream();
                    reader = new BinaryReader(netStream);
                    writer = new BinaryWriter(netStream);

                    thread = new Thread(readMsgThread);
                    thread.Start();
                }
                catch (Exception)
                {
                    closeConnected();
                    throw;
                }
            }
        }

        /// <summary>
        /// 解构函数
        /// </summary>
        ~NetTcpClient()
        {
            closeConnected();
        }

        /// <summary>
        /// 读取数据线程
        /// </summary>
        private void readMsgThread()
        {
            while (reader != null)
            {
                try
                {
                    string sMsg = reader.ReadString();
                    OnReaderMsg(sMsg);
                }
                catch (Exception)
                {
                    closeConnected();
                }
            }
        }

        /// <summary>
        /// 关闭网络连接
        /// </summary>
        public void closeConnected()
        {
            lock (lockObj)
            {
                if (tcpCLient != null)
                {
                    tcpCLient.Close();
                    tcpCLient = null;
                }
                if (reader != null)
                {
                    reader.Close();
                    reader = null;
                }
                if (writer != null)
                {
                    writer.Close();
                    writer = null;
                }
            }
            if (bConnected)
            {
                bConnected = false;
                OnConState(new ConStateEventArgs(bConnected));
            }
        }
    }

    public class ConStateEventArgs : EventArgs
    {
        public readonly bool bConnected;
        public ConStateEventArgs(bool bConnected)
        {
            this.bConnected = bConnected;
        }
    }
}
