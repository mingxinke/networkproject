﻿namespace NetworkClient
{
    partial class Form1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_connect = new System.Windows.Forms.Button();
            this.btn_disconnect = new System.Windows.Forms.Button();
            this.tBox_send = new System.Windows.Forms.TextBox();
            this.btn_send = new System.Windows.Forms.Button();
            this.tBox_ip = new System.Windows.Forms.TextBox();
            this.lable1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tBox_port = new System.Windows.Forms.TextBox();
            this.statusStrip_Info = new System.Windows.Forms.StatusStrip();
            this.toolStripLab_state = new System.Windows.Forms.ToolStripStatusLabel();
            this.listBox_receive = new System.Windows.Forms.ListBox();
            this.statusStrip_Info.SuspendLayout();
            this.SuspendLayout();
            // 
            // btn_connect
            // 
            this.btn_connect.Location = new System.Drawing.Point(340, 4);
            this.btn_connect.Name = "btn_connect";
            this.btn_connect.Size = new System.Drawing.Size(75, 23);
            this.btn_connect.TabIndex = 0;
            this.btn_connect.Text = "连接服务器";
            this.btn_connect.UseVisualStyleBackColor = true;
            this.btn_connect.Click += new System.EventHandler(this.btn_connect_Click);
            // 
            // btn_disconnect
            // 
            this.btn_disconnect.Location = new System.Drawing.Point(421, 4);
            this.btn_disconnect.Name = "btn_disconnect";
            this.btn_disconnect.Size = new System.Drawing.Size(75, 23);
            this.btn_disconnect.TabIndex = 1;
            this.btn_disconnect.Text = "断开服务器";
            this.btn_disconnect.UseVisualStyleBackColor = true;
            this.btn_disconnect.Click += new System.EventHandler(this.btn_disconnect_Click);
            // 
            // tBox_send
            // 
            this.tBox_send.Location = new System.Drawing.Point(4, 415);
            this.tBox_send.Multiline = true;
            this.tBox_send.Name = "tBox_send";
            this.tBox_send.Size = new System.Drawing.Size(723, 23);
            this.tBox_send.TabIndex = 3;
            // 
            // btn_send
            // 
            this.btn_send.Location = new System.Drawing.Point(733, 415);
            this.btn_send.Name = "btn_send";
            this.btn_send.Size = new System.Drawing.Size(75, 23);
            this.btn_send.TabIndex = 4;
            this.btn_send.Text = "button3";
            this.btn_send.UseVisualStyleBackColor = true;
            this.btn_send.Click += new System.EventHandler(this.btn_send_Click);
            // 
            // tBox_ip
            // 
            this.tBox_ip.Location = new System.Drawing.Point(87, 6);
            this.tBox_ip.Name = "tBox_ip";
            this.tBox_ip.Size = new System.Drawing.Size(100, 21);
            this.tBox_ip.TabIndex = 5;
            this.tBox_ip.Text = "127.0.0.1";
            // 
            // lable1
            // 
            this.lable1.AutoSize = true;
            this.lable1.Location = new System.Drawing.Point(4, 12);
            this.lable1.Name = "lable1";
            this.lable1.Size = new System.Drawing.Size(77, 12);
            this.lable1.TabIndex = 6;
            this.lable1.Text = "服务器地址：";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(194, 10);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 12);
            this.label2.TabIndex = 7;
            this.label2.Text = "端口";
            // 
            // tBox_port
            // 
            this.tBox_port.Location = new System.Drawing.Point(229, 6);
            this.tBox_port.Name = "tBox_port";
            this.tBox_port.Size = new System.Drawing.Size(100, 21);
            this.tBox_port.TabIndex = 5;
            this.tBox_port.Text = "52521";
            // 
            // statusStrip_Info
            // 
            this.statusStrip_Info.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripLab_state});
            this.statusStrip_Info.Location = new System.Drawing.Point(0, 441);
            this.statusStrip_Info.Name = "statusStrip_Info";
            this.statusStrip_Info.Size = new System.Drawing.Size(812, 22);
            this.statusStrip_Info.TabIndex = 8;
            this.statusStrip_Info.Text = "statusStrip1";
            // 
            // toolStripLab_state
            // 
            this.toolStripLab_state.Name = "toolStripLab_state";
            this.toolStripLab_state.Size = new System.Drawing.Size(32, 17);
            this.toolStripLab_state.Text = "就绪";
            // 
            // listBox_receive
            // 
            this.listBox_receive.FormattingEnabled = true;
            this.listBox_receive.ItemHeight = 12;
            this.listBox_receive.Location = new System.Drawing.Point(6, 28);
            this.listBox_receive.Name = "listBox_receive";
            this.listBox_receive.Size = new System.Drawing.Size(802, 376);
            this.listBox_receive.TabIndex = 9;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(812, 463);
            this.Controls.Add(this.listBox_receive);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lable1);
            this.Controls.Add(this.tBox_port);
            this.Controls.Add(this.tBox_ip);
            this.Controls.Add(this.btn_send);
            this.Controls.Add(this.tBox_send);
            this.Controls.Add(this.btn_disconnect);
            this.Controls.Add(this.btn_connect);
            this.Controls.Add(this.statusStrip_Info);
            this.Name = "Form1";
            this.Text = "Form1";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form1_FormClosed);
            this.statusStrip_Info.ResumeLayout(false);
            this.statusStrip_Info.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_connect;
        private System.Windows.Forms.Button btn_disconnect;
        private System.Windows.Forms.TextBox tBox_send;
        private System.Windows.Forms.Button btn_send;
        private System.Windows.Forms.TextBox tBox_ip;
        private System.Windows.Forms.Label lable1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tBox_port;
        private System.Windows.Forms.StatusStrip statusStrip_Info;
        private System.Windows.Forms.ToolStripStatusLabel toolStripLab_state;
        private System.Windows.Forms.ListBox listBox_receive;
    }
}

